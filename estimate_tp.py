#!/usr/bin/env python

import json
import os
import sys

from common import *
import vge_joblist_csv
import convert


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('usage: {} vge_output_directory'.format(sys.argv[0]))
        sys.exit(1)

    print('Estimation of the target problem performance')
    print('')

    config_file = os.path.dirname(__file__) + '/config.json'
    print('config: {}'.format(os.path.abspath(config_file)))
    with open(config_file) as f:
        config = json.load(f)

    print('vge_output: {}'.format(os.path.abspath(sys.argv[1])))
    vge_output_dir = sys.argv[1] + '/'
    input_csv = vge_output_dir + 'vge_joblist.csv'

    job_list, job_index = vge_joblist_csv.parse(input_csv)

    time = {}

    time['3a'] = convert.fastq_splitter(job_index['fastq_splitter'], job_list,
                                config['conversion']['3a'], vge_output_dir)

    time['4a'] = convert.bwa_align(job_index['bwa_align'], job_list,
                                config['conversion']['4a'], vge_output_dir)

    time['5a'] = convert.markduplicates(job_index['markduplicates'], job_list,
                                config['conversion']['5a'], vge_output_dir)

    time['6a'] = convert.mutation_call(job_index['mutation_call'], job_list,
                                config['conversion']['6a'], vge_output_dir)

    time['7a'] = convert.mutation_merge(job_index['mutation_merge'], job_list,
                                config['conversion']['7a'], vge_output_dir)

    time['8a'] = convert.bam_merge(job_index['bam_merge'], job_list,
                                config['conversion']['8a'], vge_output_dir)

    print_job_title('Total')

    print('time_total = time_3a + time_4a + time_5a + max(time_6a + time_7a, time_8a)')
    print('')

    for task in ['3a', '4a', '5a', '6a', '7a', '8a']:
        print('  time_{} = {:8.2f}'.format(task, time[task]))

    print('')
    time_6a_7a = time['6a'] + time['7a']
    if (time_6a_7a > time['8a']):
        print('  time_6a + time_7a = {:.2f} > time_8a'.format(time_6a_7a))
        time['max(6a+7a,8a)'] = time_6a_7a
    else:
        print('  time_6a + time_7a = {:.2f} < time_8a'.format(time_6a_7a))
        time['max(6a+7a,8a)'] = time['8a']

    time_total = 0
    for task in ['3a', '4a', '5a', 'max(6a+7a,8a)']:
        time_total += time[task]

    print('')
    print('time_total = {:.2f}'.format(time_total))

    systems = config['systems']
    systems['fugaku']['time'] = time_total

    print_job_title('Comparison')

    for s in ['k_baseline', 'fugaku']:
        system = systems[s]
        system['time_day'] = system['time'] / 60 / 60 / 24
        system['n_pipeline'] = system['node_total'] // system['node_used']
        system['n_sample'] = 1.0 / system['time_day'] * system['n_pipeline']

    improve_rate = systems['fugaku']['n_sample'] / systems['k_baseline']['n_sample']

    s_fmt = ' {:<24}| {:>10}| {:>10}'
    f_fmt = ' {:<24}| {:10.2f}| {:10.2f}'
    d_fmt = ' {:<24}| {:10d}| {:10d}'
    print(s_fmt.format('', 'K baseline', 'Fugaku'))
    print('-' * 50)
    print(f_fmt.format('1 pipeline speed [day]',
                       systems['k_baseline']['time_day'],
                       systems['fugaku']['time_day']))
    print(d_fmt.format('# of nodes / pipeline',
                       systems['k_baseline']['node_used'],
                       systems['fugaku']['node_used']))
    print(d_fmt.format('# of pipelines',
                       systems['k_baseline']['n_pipeline'],
                       systems['fugaku']['n_pipeline']))
    print(f_fmt.format('# of samples / day',
                       systems['k_baseline']['n_sample'],
                       systems['fugaku']['n_sample']))
    print('-' * 50)
    print(f_fmt.format('improvement rate',
                       1.0,
                       improve_rate))
    print('')
