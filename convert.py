#!/usr/bin/env python

import itertools

from common import *


def find_max_time_job(job_index, job_list):
    t_max = 0
    id_max = -1
    for id in itertools.chain(*job_index):
        t = job_list[id]['time']
        if (t > t_max):
            t_max = t
            id_max = id
    return id_max, t_max


def convert_max_time_job(job_index, job_list, conv_factor, vge_output_dir):
    id_max, t_max = find_max_time_job(job_index, job_list)
    print('slowest job: {}'.format(id_max))
    file = job_list[id_max]['file'] + '.e' + str(id_max)
    print('read timing data from:')
    print('  {}'.format(file))
    time_list = get_elapsed_time(vge_output_dir + file)
    return convert_time(time_list, conv_factor, t_max)


def find_max_time_worker(job_index, job_list):
    worker_job_list = {}
    for id in itertools.chain(*job_index):
        w = job_list[id]['worker']
        t = job_list[id]['time']
        jobs = worker_job_list.setdefault(w, [0.0, []])
        jobs[0] += t
        jobs[1].append(id)
    t_max = 0.0
    w_max = -1
    for w in worker_job_list.keys():
        if worker_job_list[w][0] > t_max:
            t_max = worker_job_list[w][0]
            w_max = w
    assert w_max > 0
    return w_max, t_max, worker_job_list[w_max][1]


def fastq_splitter(job_index, job_list, conv_factor, vge_output_dir):
    print_job_title('3A: fastq_splitter')
    return convert_max_time_job(job_index, job_list, conv_factor, vge_output_dir)


def bwa_align(job_index, job_list, conv_factor, vge_output_dir):
    print_job_title('4A: bwa_align')

    busiest_worker, t_max, busiest_workers_jobs = find_max_time_worker(job_index, job_list)
    print('busiest worker: {}'.format(busiest_worker))
    print('accepted jobs: {}'.format(str(busiest_workers_jobs)))
    print('read timing data from:')
    
    time_list = {}
    for i, id in enumerate(busiest_workers_jobs):
        file = job_list[id]['file'] + '.e' + str(id)
        print('  {}'.format(file))
        if i == 0:
            time_list = get_elapsed_time(vge_output_dir + file)
        else:
            t_list = get_elapsed_time(vge_output_dir + file)
            for i in range(len(t_list)):
                assert time_list[i][0] == t_list[i][0]
                time_list[i][1] += t_list[i][1]

    return  convert_time(time_list, conv_factor, t_max)


def markduplicates(job_index, job_list, conv_factor, vge_output_dir):
    print_job_title('5A: markduplicates')
    return convert_max_time_job(job_index, job_list, conv_factor, vge_output_dir)


def mutation_call(job_index, job_list, conv_factor, vge_output_dir):
    print_job_title('6A: mutation_call')
    return convert_max_time_job(job_index, job_list, conv_factor, vge_output_dir)


def mutation_merge(job_index, job_list, conv_factor, vge_output_dir):
    print_job_title('7A: mutation_merge')
    return convert_max_time_job(job_index, job_list, conv_factor, vge_output_dir)


def bam_merge(job_index, job_list, conv_factor, vge_output_dir):
    print_job_title('8A: bam_merge')
    return convert_max_time_job(job_index, job_list, conv_factor, vge_output_dir)
