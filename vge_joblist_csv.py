#!/usr/bin/env python

import re
import sys


re_job_name = re.compile('(.*)_\d{8}_\d{4}_\d{6}\.sh\.\d+')


def get_job_name(s):
    return re_job_name.match(s).group(1)


def parse(file):
    with open(file) as f:
        f.readline()  # skip header

        job_index = {}
        job_list = []
        bulkjob_list = []
        num_job = 0
        num_unique_job = 0
    
        for line in f:
            items = line.rstrip().split(',')
            if len(items) == 15:    # VGE1
                (jobid, status, sendvgetime, bulkjob_id,
                finish_time, start_time, worker, return_code,
                filename, elapsed_time, genomon_pid, max_task,
                command_id, unique_jobid, sendtoworker) = items
            elif len(items) == 17:    # VGE2
                (jobid, status, sendvgetime, bulkjob_id, execjobid,
                finish_time, start_time, worker, return_code,
                filename, pipeline_parent_pid, elapsed_time, max_task, pipeline_pid,
                command_id, unique_jobid, sendtoworker) = items
            elif len(items) == 18:    # VGE2_mod
                (jobid, status, sendvgetime, command_id, bulkjob_id,
                execjobid, finish_time, start_time, worker, return_code,
                filename, pipeline_parent_pid, elapsed_time, max_task,
                pipeline_pid,sendtoworker,unique_jobid,priority) = items
            else:
                raise RuntimeError("invalid line: %s" % line)

            assert return_code == '0'

            jobid = int(jobid)
            bulkjob_id = int(bulkjob_id)
            unique_jobid = int(unique_jobid)
            worker = int(worker)
    
            assert jobid == num_job
            num_job += 1
    
            time = float(elapsed_time)

            job = dict(id = jobid, file = filename, time = time, worker =  worker,
                       bulkjob_id = bulkjob_id)
            job_list.append(job)

            if bulkjob_id == 0:
                assert unique_jobid == num_unique_job
                num_unique_job += 1
    
                bulkjob_list.append([])
    
                name = get_job_name(filename)
               #job_index[name].append(unique_jobid)
                job_index.setdefault(name, []).append(unique_jobid)

            bulkjob_list[unique_jobid].append(jobid)

        # unique_jobid list -> jobid list
        for name, unique_joblist in job_index.items():
            tmp = []
            for unique_job in unique_joblist:
                tmp.append(bulkjob_list[unique_job])
            job_index[name] = tmp
    
        return job_list, job_index


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print('usage: %s vge_output_directory' % sys.argv[0])
        sys.exit(1)

    print('vge_output: %s' % sys.argv[1])
    vge_output_dir = sys.argv[1] + '/'
    input_csv = vge_output_dir + 'vge_joblist.csv'

    job_list, job_index = parse(input_csv)
