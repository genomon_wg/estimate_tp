Estimation of the target problem performance

config: /Users/suzukis/estimate_tp2/config.json
vge_output: /Users/suzukis/fugaku/run_ATL_llio_sc6_preload_new_rrs.sh.1749978/vge_output

--------------------------------------------------------------------------------
==== 3A: fastq_splitter ====

slowest job: 2
read timing data from:
  fastq_splitter_20201028_2230_191267.sh.0.e2

count_lines            :   347.31 x <3.09> ==>  1073.18
split_file             :  1146.18 x <3.09> ==>  3541.70
*rest*                 :     0.04 x <1.00> ==>     0.04

total   4614.92

--------------------------------------------------------------------------------
==== 4A: bwa_align ====

busiest worker: 347
accepted jobs: [45, 688, 1093, 1417, 1921, 2201, 2653, 3154, 3569, 3982, 4365, 4479, 4863, 5309]
read timing data from:
  bwa_align_20201028_2255_811485.sh.41.e45
  bwa_align_20201028_2255_811485.sh.684.e688
  bwa_align_20201028_2255_811485.sh.1089.e1093
  bwa_align_20201028_2255_811485.sh.1413.e1417
  bwa_align_20201028_2255_811485.sh.1917.e1921
  bwa_align_20201028_2255_811485.sh.2197.e2201
  bwa_align_20201028_2255_327992.sh.317.e2653
  bwa_align_20201028_2255_327992.sh.818.e3154
  bwa_align_20201028_2255_327992.sh.1233.e3569
  bwa_align_20201028_2255_327992.sh.1646.e3982
  bwa_align_20201028_2255_327992.sh.2029.e4365
  bwa_align_20201028_2255_327992.sh.2143.e4479
  bwa_align_20201028_2255_327992.sh.2527.e4863
  bwa_align_20201028_2255_327992.sh.2973.e5309

bwa_mem                :  1387.29 x <1.50> x ceil(<2.06>) ==>  6242.82
scatter_sam            :   112.19 x <1.50> x ceil(<2.06>) ==>   504.87
*rest*                 :     5.71 x <1.50> x ceil(<2.06>) ==>    25.70

total   6773.40

--------------------------------------------------------------------------------
==== 5A: markduplicates ====

slowest job: 5515
read timing data from:
  markduplicates_20201028_2321_274386.sh.0.e5515

cat                    :   347.89 x <3.09> ==>  1074.98
bamsort                :  1396.98 x <2.06> ==>  2877.78
bammarkduplicates      :  3351.92 x <2.06> ==>  6904.96
remove_chr_dir         :    21.83 x <2.06> ==>    44.98
*rest*                 :     0.52 x <3.09> ==>     1.59

total  10904.28

--------------------------------------------------------------------------------
==== 6A: mutation_call ====

slowest job: 5672
read timing data from:
  mutation_call_20201029_0047_068139.sh.68.e5672

fisher comparison      :   684.37 x <3.09> ==>  2114.69
mutfilter realignment  :   116.46 x <3.09> ==>   359.85
mutfilter indel        :   145.90 x <3.09> ==>   450.84
mutfilter breakpoint   :   120.92 x <3.09> ==>   373.65
mutfilter simplerepeat :     6.21 x <3.09> ==>    19.18
EBFilter               :   432.09 x <3.09> ==>  1335.17
cp                     :     0.01 x <3.09> ==>     0.04
*rest*                 :     0.30 x <1.00> ==>     0.30

total   4653.71

--------------------------------------------------------------------------------
==== 7A: mutation_merge ====

slowest job: 5879
read timing data from:
  mutation_merge_20201029_0113_551356.sh.0.e5879

cat                    :     2.56 x <1.00> ==>     2.56
*rest*                 :     0.08 x <1.00> ==>     0.08

total      2.65

--------------------------------------------------------------------------------
==== 8A: bam_merge ====

slowest job: 5603
read timing data from:
  bam_merge_20201029_0047_142448.sh.0.e5603

samtools cat           :   524.58 x <3.09> ==>  1620.95
bai_merge              :     4.62 x <2.06> ==>     9.51
md5sum                 :   636.11 x <3.09> ==>  1965.58
*rest*                 :     0.25 x <1.00> ==>     0.25

total   3596.29

--------------------------------------------------------------------------------
==== Total ====

time_total = time_3a + time_4a + time_5a + max(time_6a + time_7a, time_8a)

  time_3a =  4614.92
  time_4a =  6773.40
  time_5a = 10904.28
  time_6a =  4653.71
  time_7a =     2.65
  time_8a =  3596.29

  time_6a + time_7a = 4656.36 > time_8a

time_total = 26948.96

--------------------------------------------------------------------------------
==== Comparison ====

                         | K baseline|     Fugaku
--------------------------------------------------
 1 pipeline speed [day]  |       8.84|       0.31
 # of nodes / pipeline   |         36|         96
 # of pipelines          |       2304|       1656
 # of samples / day      |     260.68|    5309.24
--------------------------------------------------
 improvement rate        |       1.00|      20.37

